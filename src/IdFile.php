<?php

declare(strict_types=1);

namespace ZdenekGebauer\IdFile;

use InvalidArgumentException;

use function in_array;
use function is_array;
use function strlen;

class IdFile
{
    final public const FILES_PER_DIR_UNKNOWN = -1, // unknown number of files per directory
        FILES_PER_DIR_UNLIMITED = 0, // all files in base directory, no subdirectories
        FILES_PER_DIR_10 = 1, // max 10 files per directory
        FILES_PER_DIR_100 = 2, // max 100 files per directory
        FILES_PER_DIR_1000 = 3; // max 1 000 files per directory

    /**
     * create new filename from specified id and original filename
     */
    public static function createFileName(int $fileId, string $fileName): string
    {
        if ($fileName === '') {
            throw new InvalidArgumentException('filename cannot be empty');
        }
        return self::fileId($fileId) . self::slugify(pathinfo($fileName, PATHINFO_BASENAME));
    }

    public static function slugify(string $string): string
    {
        $convert = [
            'á' => 'a',
            'Á' => 'a',
            'ä' => 'a',
            'Ä' => 'a',
            'ą' => 'a',
            'Ą' => 'a',
            'č' => 'c',
            'Č' => 'c',
            'ď' => 'd',
            'Ď' => 'd',
            'é' => 'e',
            'É' => 'e',
            'ě' => 'e',
            'Ě' => 'e',
            'ę' => 'e',
            'Ę' => 'e',
            'ē' => 'e',
            'Ē' => 'e',
            'ë' => 'e',
            'Ë' => 'e',
            'í' => 'i',
            'Í' => 'i',
            'ľ' => 'l',
            'Ľ' => 'l',
            'ł' => 'l',
            'Ł' => 'l',
            'ĺ' => 'l',
            'Ĺ' => 'l',
            'ň' => 'n',
            'Ň' => 'n',
            'ó' => 'o',
            'Ó' => 'o',
            'ö' => 'o',
            'Ö' => 'o',
            'ő' => 'o',
            'Ő' => 'o',
            'ř' => 'r',
            'Ř' => 'r',
            'š' => 's',
            'Š' => 's',
            'ť' => 't',
            'Ť' => 't',
            'ú' => 'u',
            'Ú' => 'u',
            'ů' => 'u',
            'Ů' => 'u',
            'ű' => 'u',
            'Ű' => 'u',
            'ü' => 'u',
            'Ü' => 'u',
            'ý' => 'y',
            'Ý' => 'y',
            'ž' => 'z',
            'Ž' => 'z',
        ];
        $string = strtr($string, $convert);
        $string = (string)preg_replace('/[^a-zA-Z0-9_.]+/', '-', $string);
        return trim($string, '-');
    }

    /**
     * delete file(s) specified by $fileId
     */
    public static function delete(
        int $fileId,
        string $baseDir,
        string $ignoreFileName = '',
        int $filesPerDir = self::FILES_PER_DIR_UNLIMITED
    ): void {
        $options = [self::FILES_PER_DIR_UNLIMITED];
        if (in_array($filesPerDir, self::allowedFilesPerDir(), true)) {
            $options = [$filesPerDir];
        } elseif ($filesPerDir === self::FILES_PER_DIR_UNKNOWN) {
            $options = self::allowedFilesPerDir();
        }

        $baseDir = rtrim($baseDir, '/') . '/';
        foreach ($options as $option) {
            $dir = $baseDir . self::idToPath($fileId, $option);
            $files = glob($dir . self::fileId($fileId) . '*');
            $files = is_array($files) ? $files : [];
            foreach ($files as $fullPath) {
                if ($ignoreFileName !== '' && basename($fullPath) === $ignoreFileName) {
                    continue;
                }
                unlink($fullPath);
            }
        }
    }

    /**
     * @return int[]
     */
    private static function allowedFilesPerDir(): array
    {
        return [
            self::FILES_PER_DIR_1000,
            self::FILES_PER_DIR_100,
            self::FILES_PER_DIR_10,
            self::FILES_PER_DIR_UNLIMITED,
        ];
    }

    /**
     * returns relative directory path created from id by number of files per directory with trailing slash
     */
    public static function idToPath(int $fileId, int $filesPerDir = self::FILES_PER_DIR_UNLIMITED): string
    {
        $allow = self::allowedFilesPerDir();
        if (!in_array($filesPerDir, $allow, true)) {
            throw new InvalidArgumentException('not allowed files per directory:' . $filesPerDir);
        }

        if ($filesPerDir === self::FILES_PER_DIR_UNLIMITED) {
            return '';
        }

        $tmp = [];
        $strId = (string)$fileId;
        $files = self::FILES_PER_DIR_UNLIMITED;
        if (in_array($filesPerDir, $allow, true)) {
            $files = $filesPerDir;
        }

        $length = strlen($strId) - $files;
        for ($pos = 0; $pos < $length; $pos++) {
            $tmp[] = $strId[$pos];
        }
        $dir = implode('/', $tmp);
        return $dir . ($dir === '' ? '' : '/');
    }

    private static function fileId(int $fileId): string
    {
        return $fileId . '_';
    }

    /**
     * returns full filename with specified id
     *
     * Parameter $eFilesPerDir should be set to FILES_PER_DIR*.
     * Special value FILES_PER_DIR_UNKNOWN means 'search in all possible subdirectories' (slower, use with caution).
     * Returns empty string if file is not found.
     */
    public static function findFile(
        int $fileId,
        string $baseDir,
        int $filesPerDir = self::FILES_PER_DIR_UNLIMITED
    ): string {
        $options = [self::FILES_PER_DIR_UNLIMITED];
        if (in_array($filesPerDir, self::allowedFilesPerDir(), true)) {
            $options = [$filesPerDir];
        } elseif ($filesPerDir === self::FILES_PER_DIR_UNKNOWN) {
            $options = self::allowedFilesPerDir();
        }

        $baseDir = rtrim($baseDir, '/') . '/';
        foreach ($options as $option) {
            $files = glob($baseDir . self::idToPath($fileId, $option) . self::fileId($fileId) . '*');
            if (is_array($files) && isset($files[0])) {
                return $files[0];
            }
        }
        return '';
    }

    /**
     * returns filename without id prefix (assume filename in format id_filename.ext)
     */
    public static function stripId(string $fileName): string
    {
        $parts = explode('_', $fileName, 2);
        return $parts[1] ?? $parts[0];
    }
}
