# Id File

Library of functions for manipulating with files prefixed with numeric identifier (ie. 1234_file.jpg).

Files can be stored in subdirectories, based on id (ie. /images/1/2/123456_another-image.gif). Number of files in each
subdirectory can be set to 10, 100 1000 or unlimited quantity

## Usage

### Find files

Expected files stored in subdirectories like:

```
/images/1_image.jpg
/images/1/1234_another-image.gif
/images/1/2/12345_image.gif
```

```php 
IdFile::findFile(1, '/images', IdFile::FILES_PER_DIR_1000)); 
// returns /images/1_image.jpg, expects max 1000 files/directory
  
IdFile::findFile(1234, '/images'));
// returns /images/1/1234_another-image.gif. it is slower, because search subdirectories for all options  (10/100/100 per directory) 
```

### Create file name with identifier

```php 
IdFile::createFielName(1, 'file.jpg)); // returns 1_image.jpg
IdFile::createFielName(1234, 'file with spaces.tar.gz)); // returns 1234_file-with-spaces.tar.gz
IdFile::createFielName(0, 'příliš žluťoučký kůň.txt)); // returns 0_prilis-zlutoucky-kun.txt
```

### Get file name without identifier

```php 
IdFile::stripId('1_file.jpg)); // returns file.jpg
IdFile::stripId('1234_file-with-spaces.tar.gz') // returns file-with-spaces.tar.gz 
```

### Create path to subdirectory

```php
IdFile::idToPath(123456, IdFile::FILES_PER_DIR_1000); // returns '1/2/3/' 
IdFile::idToPath(123456, IdFile::FILES_PER_DIR_100); // returns '1/2/3/4/'
IdFile::idToPath(123456, IdFile::FILES_PER_DIR_10); // returns '1/2/3/4/5/'  
IdFile::idToPath(123456, IdFile::FILES_PER_DIR_UNLIMITED); // returns ''
```      

### Delete file

Expected files stored in subdirectories like:

```
/images/1/2/3/123456_file.jpg
/images/1/2/3/123456_file.gif
/images/1/2/3/123456_file.png
```

```php
IdFile::delete(123456, '/images', '', IdFile::FILES_PER_DIR_1000); // delete both files
IdFile::delete(123456, '/images', '123456_file.gif', IdFile::FILES_PER_DIR_1000); // delete files except 123456_file.gif
IdFile::delete(123456, '/images', ''); // delete files 123456_* in all subdirectories, slower 
```
