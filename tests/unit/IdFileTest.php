<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use InvalidArgumentException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Tests\Support\UnitTester;
use ZdenekGebauer\IdFile\IdFile;

class IdFileTest extends Unit
{
    protected UnitTester $tester;

    private string $dir;

    protected function _before()
    {
        $this->dir = codecept_data_dir('temp');
        $this->deleteTestDir();
        mkdir($this->dir);
    }

    protected function _after()
    {
        $this->deleteTestDir();
    }

    private function deleteTestDir(): void
    {
        clearstatcache();
        if (is_dir($this->dir)) {
            $iterator = new RecursiveDirectoryIterator($this->dir, RecursiveDirectoryIterator::SKIP_DOTS | \FilesystemIterator::SKIP_DOTS);
            foreach (new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST) as $file) {
                if ($file->isDir()) {
                    rmdir($file->getPathname());
                }
                if ($file->isFile()) {
                    unlink($file->getPathname());
                }
            }
            rmdir($this->dir);
        }
    }

    public function testCreateFileName(): void
    {
        $this->tester->assertEquals('1_file.jpg', IdFile::createFileName(1, 'file.jpg'));
        $this->tester->assertEquals('0_file.jpg', IdFile::createFileName(0, 'file.jpg'));

        $this->tester->assertEquals(
            '99999_Prilis-zlutoucky-kun.tar.gz',
            IdFile::createFileName(99999, 'Příliš žluťoučký kůň.tar.gz')
        );
    }

    public function testCreateFileNameWithEmptyFile(): void
    {
        $this->tester->expectThrowable(
            new InvalidArgumentException('filename cannot be empty'),
            static function () {
                IdFile::createFileName(1, '');
            }
        );
    }

    public function testStripId(): void
    {
        $this->tester->assertEquals('file.jpg', IdFile::stripId('file.jpg'));
        $this->tester->assertEquals('file.jpg', IdFile::stripId('0_file.jpg'));
        $this->tester->assertEquals('file.tar.gz', IdFile::stripId('22_file.tar.gz'));
    }

    public function testIdToPath(): void
    {
        $this->tester->assertEquals('', IdFile::idToPath(123456, IdFile::FILES_PER_DIR_UNLIMITED));
        $this->tester->assertEquals('1/2/3/', IdFile::idToPath(123456, IdFile::FILES_PER_DIR_1000));
        $this->tester->assertEquals('1/2/3/4/', IdFile::idToPath(123456, IdFile::FILES_PER_DIR_100));
        $this->tester->assertEquals('1/2/3/4/5/', IdFile::idToPath(123456, IdFile::FILES_PER_DIR_10));
    }

    public function testIdToPathWithInvalidParameter(): void
    {
        $this->tester->expectThrowable(
            new InvalidArgumentException('not allowed files per directory:-1'),
            static function () {
                IdFile::idToPath(123456, -1);
            }
        );
    }

    public function testFindFile(): void
    {
        $baseDir = $this->dir . '/';

        file_put_contents($baseDir . '1_file.jpg', '');
        $expect = $this->normalizeDirDelimiters($baseDir . '1_file.jpg');
        $output = $this->normalizeDirDelimiters(IdFile::findFile(1, $baseDir));
        $this->tester->assertEquals($expect, $output);
        $this->tester->assertEquals('', IdFile::findFile(11, $baseDir));

        mkdir($baseDir . '1/2/', 0777, true);
        file_put_contents($baseDir . '1/2/123_file.jpg', '');
        $expect = $this->normalizeDirDelimiters($baseDir . '/1/2/123_file.jpg');
        $output = $this->normalizeDirDelimiters(IdFile::findFile(123, $baseDir, IdFile::FILES_PER_DIR_10));
        $this->tester->assertEquals($expect, $output);
        $output = $this->normalizeDirDelimiters(IdFile::findFile(123, $baseDir, IdFile::FILES_PER_DIR_UNKNOWN));
        $this->tester->assertEquals($expect, $output);
        $this->tester->assertEquals('', IdFile::findFile(12, $baseDir));

        mkdir($baseDir . '1/2/3/', 0777, true);
        file_put_contents($baseDir . '1/2/3/123456_file.jpg', '');
        $expect = $this->normalizeDirDelimiters($baseDir . '/1/2/3/123456_file.jpg');
        $output = $this->normalizeDirDelimiters(IdFile::findFile(123456, $baseDir, IdFile::FILES_PER_DIR_1000));
        $this->tester->assertEquals($expect, $output);
        $output = $this->normalizeDirDelimiters(IdFile::findFile(123456, $baseDir, IdFile::FILES_PER_DIR_UNKNOWN));
        $this->tester->assertEquals($expect, $output);
    }

    private function normalizeDirDelimiters($path)
    {
        return str_replace(['\\', '//'], '/', (string) $path);
    }

    public function testDelete(): void
    {
        $baseDir = $this->dir . '/';
        file_put_contents($baseDir . '/1_file.jpg', '');
        mkdir($baseDir . '1/2/', 0777, true);
        file_put_contents($baseDir . '1/2/123_file.jpg', '');
        mkdir($baseDir . '1/2/3/', 0777, true);

        file_put_contents($baseDir . '1/2/3/123456_file.jpg', '');
        file_put_contents($baseDir . '1/2/3/123456_file.gif', '');
        IdFile::delete(123456, $baseDir, '123456_file.gif', IdFile::FILES_PER_DIR_1000);
        $this->tester->assertFileExists($baseDir . '/1_file.jpg');
        $this->tester->assertFileExists($baseDir . '1/2/123_file.jpg');
        $this->tester->assertFileNotExists($baseDir . '1/2/3/123456_file.jpg');
        $this->tester->assertFileExists($baseDir . '1/2/3/123456_file.gif');

        file_put_contents($baseDir . '1/2/3/123456_file.png', '');
        IdFile::delete(123456, $baseDir, '', IdFile::FILES_PER_DIR_1000);
        $this->tester->assertFileNotExists($baseDir . '1/2/3/123456_file.gif');
        $this->tester->assertFileNotExists($baseDir . '1/2/3/123456_file.png');

        IdFile::delete(123, $baseDir, '', IdFile::FILES_PER_DIR_UNKNOWN);
        $this->tester->assertFileNotExists($baseDir . '1/2/123_file.jpg');
    }
}
